# -*- coding: utf-8 -*-
#/usr/bin/ruby

require 'zlib'

class Record
end

class Respondent < Record
end

class Pregnancy < Record
end

class Table
  attr_accessor :records
  def initialize
    @records = []
  end

  def __len__
    @records.size
  end

  def read_file(data_dir, filename, fields, constructor, n=nil)
    filename = File.join(data_dir, filename)
    fp = nil
    if filename.slice(-2, 2) == 'gz'
      fp = Zlib::GzipReader.open(filename)
    else
      fp = File.open(filename)
    end
    fp.each_line {|line|
      record = make_record(line, fields, constructor)
      add_record(record)
    }
    fp.close()
  end

  def make_record(line, fields, constructor)
    obj = constructor.new
    fields.each do |field, start, e, cast|
      begin
        s = line[start-1, e-(start-1)] # line[start-1:end] in python
        if (cast == Integer)
          val = s.to_i
        elsif (cast == Float)
          val = s.to_f
        else
          raise NotImplementedError
        end
      rescue
        val = 'NA'
      end
      obj.instance_variable_set("@"+field, val) # setattr(obj, field, val) in python
      constructor.class_eval("attr_reader :#{field}")
    end
    obj
  end

  def add_record(record)
    @records.push(record)
  end

  def extend_records(records)
    @records += records
  end

  def recode
    nil
  end
end

class Respondents < Table
  def read_records(data_dir='.', n=nil)
    filename = get_file_name()
    read_file(data_dir, filename, get_fields(), Respondent, n)
    recode()
  end

  def get_file_name
    '2002FemResp.dat.gz'
  end

  def get_fields
    [
      ['caseid', 1, 12, Integer],
    ]

  end
end

class Pregnancies < Table
  def read_records(data_dir='.', n=nil)
    filename = get_file_name()
    read_file(data_dir, filename, get_fields(), Pregnancy, n)
  end

  def get_file_name
    '2002FemPreg.dat.gz'
  end

  def get_fields
    [
      ['caseid', 1, 12, Integer],
      ['nbrnaliv', 22, 22, Integer],
      ['babysex', 56, 56, Integer],
      ['birthwgt_lb', 57, 58, Integer],
      ['birthwgt_oz', 59, 60, Integer],
      ['prglength', 275, 276, Integer],
      ['outcome', 277, 277, Integer],
      ['birthord', 278, 279, Integer],
      ['agepreg', 284, 287, Integer],
      ['finalwgt', 423, 440, Float],
    ]
  end

  def recode
    @records.each {|rec|
      begin
        if rec.agepreg != 'NA'
          rec.agepreg /= 100.0
        end
      rescue
      end
    }

    begin
      if (rec.birthwgt_lb != 'NA' && rec.birthwgt_lb < 20 &&
          rec.birthwgt_oz != 'NA' && rec.birthwgt_oz <= 16)
        rec.totalwgt_oz = rec.birthwgt_lb * 16 + rec.birthwgt_oz
      else
        rec.totalwgt_oz = 'NA'
      end
    rescue
    end
  end
end

def main(data_dir='.')
  resp = Respondents.new
  resp.read_records(data_dir)
  puts 'Number of respondents', resp.records.size

  preg = Pregnancies.new
  preg.read_records(data_dir)
  puts 'Number of pregnancies', preg.records.size
end

if __FILE__ == $0
  main(ARGV)
end
