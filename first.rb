# -*- coding: utf-8 -*-
#/usr/bin/ruby

require './survey.rb'
require './thinkstats.rb'

def prglength_average(records)
  records.inject(0.0) {|r, i|
    r += i.prglength
  }/ records.size
end

table = Pregnancies.new
table.read_records
puts '妊娠レコードの総数: ', table.records.size
puts '生児出生数: ', table.records.select {|v| v.outcome == 1}.size

first = table.records.select {|v| v.birthord == 1}
another = table.records.select {|v| v.birthord >= 2}
puts '第一子:' , first.size
puts '第二子以降:' , another.size

first_length = prglength_average first
another_length = prglength_average another
puts '第一子の妊娠期間(週)', first_length
puts '第二子以降の妊娠期間(週)', another_length
puts '第一子と第二子以降の妊娠期間の差(時間)', (first_length - another_length)*7*24

# 演習問題2-1
first_prglength = first.map { |item| item.prglength}
another_prglength = another.map { |item| item.prglength}

puts '第一子の妊娠期間の標準偏差', Math.sqrt(var(first_prglength))
puts '第二子以降の妊娠期間の標準偏差', Math.sqrt(var(another_prglength))

hist = Hash.new
hist.default = 0
first_prglength.each do |item|
  hist[item] += 1
end

n = first_prglength.length.to_f
puts n
pmf = Hash.new
hist.each do |key, value|
  pmf[key] = value/n
end

p pmf
