# -*- coding: utf-8 -*-
#/usr/bin/ruby

require './thinkstats.rb'

def pumpkin
  weight = [0.5, 0.5, 1.5, 1.5, 96]
  m = mean(weight)
  v = var(weight)
  s = Math.sqrt(v)

  puts "算術平均 :", m
  puts "分散 :", v
  puts "標準偏差 :", s
end

pumpkin
