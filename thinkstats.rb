# -*- coding: utf-8 -*-
#/usr/bin/ruby

def mean(t)
  t.inject(0.0) {|r,i|
    r += i
  }/t.size
end

def mean_var(t)
  mu = mean(t)
  var = var(t)
  return mu, var
end

def trim(t, p=0.01)
  sorted = t.sort
  n = (p*t.size).to_i
  sorted = sorted(n, t.size - 2*n)
  sorted
end

def trimmed_mean(t, p=0.01)
  t = trim(t, p)
  mean(t)
end

def trimmed_mean_var(t, p=0.01)
  t = trim(t, p)
  mu, var = mean_var(t)
  return mu, var
end

def var(t, mu=nil)
  if (mu == nil)
    mu = mean(t)
  end

  dev2 = t.map do |x|
    (x - mu)**2
  end
  var = mean(dev2)
  var
end

def binom(n, k, d=[])
  if k == 0 then return 1 end
  if n == 0 then return 0 end
  begin
    return d[n, k-n+1]
  rescue
    res = binom(n-1, k) + binom(n-1, k-1)
    d[n, k-n+1] = res
    res
  end
end

class Interpolator
  attr_reader :xs, :ys
  def initialize(xs, ys)
    @xs = xs
    @ys = ys
  end

  def lookup(x)
    _bisect(x, @xs, @ys)
  end

  def reverse(y)
    _bisect(y, @ys, @xs)
  end

  def _bisect(x, xs, ys)
    if (x <= xs[0]) then return ys[0] end
    if (x >= xs[-1]) then return ys[-1] end

    i = xs.bisect(x)
    frac = 1.0 * (x - xs[i-1]) / (xs[i] - xs[i-1])
    y = ys[i-1] + frac * 1.0 * (ys[i] - ys[i-1])
    y
  end
end

# http://d.hatena.ne.jp/mopemope/20081015/p2
class Array
  def bisect(item, lo=0, hi=nil)
    if lo < 0 then raise IndexError.new('lo must be non-negative') end
    if hi == nil then hi = self.length end
    while lo < hi
      mid = ((lo+hi)/2).truncate
      if self[mid] < item
        lo = mid+1
      else
        hi = mid
      end
    end
    self.insert(lo, item)
  end
end
